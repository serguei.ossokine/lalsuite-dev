FROM debian:buster

LABEL name="AEI benchmarks - Debian Buster"
LABEL maintainer="Serguei Ossokine <serguei.ossokine@ligo.org>"
LABEL support="Unsupported"

# ensure non-interactive debian installation
ENV DEBIAN_FRONTEND noninteractive
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

# install available updates
RUN apt-get update && apt-get --assume-yes upgrade && apt-get clean

# support https repositories
RUN apt-get --assume-yes install \
      apt-transport-https \
      apt-utils \
      bash-completion \
      curl \
      lsb-release \
      wget \
      dvipng \
      texlive-latex-base \
      texlive-latex-extra && \
    apt-get clean

# Add other repos
RUN wget http://software.ligo.org/lscsoft/debian/pool/contrib/l/lscsoft-archive-keyring/lscsoft-archive-keyring_2016.06.20-2_all.deb && \
    apt --assume-yes install ./lscsoft-archive-keyring_2016.06.20-2_all.deb && \
    rm -f lscsoft-archive-keyring_2016.06.20-2_all.deb

RUN echo "deb http://software.ligo.org/gridtools/debian buster main" > /etc/apt/sources.list.d/gridtools.list && \
    echo "deb [trusted=yes] https://hypatia.aei.mpg.de/lsc-amd64-buster ./" > /etc/apt/sources.list.d/lscsoft.list


# Install the conda env
ENV PATH /opt/conda/bin:$PATH
RUN curl -O https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh  && bash Miniconda3-latest-Linux-x86_64.sh -bfp /opt/conda
RUN conda init bash && curl -O https://computing.docs.ligo.org/conda/environments/linux/igwn-py38.yaml && conda env create --file igwn-py38.yaml
RUN  bash -c "source /opt/conda/etc/profile.d/conda.sh && conda activate igwn-py38 && pip install parallel_bilby && pip install mpi4py && conda clean -a && rm Miniconda3-latest-Linux-x86_64.sh"

# Get the waveforms data
RUN curl -O https://git.ligo.org/lscsoft/lalsuite-extra/-/raw/master/data/lalsimulation/SEOBNRv4ROM_v2.0.hdf5 && mkdir -p /usr/share/lalsimulation/ && mv SEOBNRv4ROM_v2.0.hdf5 /usr/share/lalsimulation/





RUN apt-get --assume-yes install openssh-client \
    emacs \
    vim

# Get the benchmarks
RUN bash -c "source /opt/conda/etc/profile.d/conda.sh && conda activate  igwn-py38 && git clone https://github.com/SergeiOssokine/benchmark_newvulcan.git && cd benchmark_newvulcan && chmod u+x run_all* && cp * /usr/local/bin/ && cd ../ && rm -rf benchmark_newvulcan"
